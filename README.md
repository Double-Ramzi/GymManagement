# Fix glassfish jboss-logging dependency

1) Download "jboss-logging-3.3.0.FINAL.jar"
2) Put it in ${glassfish installation location}/glassfish/modules/
3) Remove "jboss-logging.jar" in ${glassfish installation location}/glassfish/modules/