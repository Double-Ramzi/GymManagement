/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

import static javax.xml.crypto.dsig.DigestMethod.SHA256;


public class PasswordEncoder {

    public static String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        return bytetoString(bytes);
    }

    private static String bytetoString(byte[] input) {
        return Base64.getEncoder().encodeToString(input);
    }

    public static String encodePassword(String raw, String salt) {
        try {

            String input = raw + salt;
            StringBuilder hash = new StringBuilder();

            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f'};

            for (byte b : hashedBytes) {
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
            return hash.toString();

        } catch (NoSuchAlgorithmException e) {

            return null;
        }
    }

    public static boolean isPasswordValid(String encoded, String raw, String salt) {
        return comparePasswords(encoded, PasswordEncoder.encodePassword(raw, salt));
    }

    private static boolean comparePasswords(String password1, String password2) {
        return password1.equals(password2);
    }
}
