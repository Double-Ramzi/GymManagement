/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import entity.Member;
import entity.Subscription;
import repository.MemberRepository;

import java.util.List;

public class MemberManager {

    private MemberRepository memberRepository = new MemberRepository();

    public Subscription getCurrentSubscription(Member member) {
        List subscriptions = this.memberRepository.foundSubscriptions(member);
        return subscriptions.size() == 0 ? null : (Subscription) subscriptions.get(0);
    }

    public Boolean isSubscribedMember(Member member) {
        return this.getCurrentSubscription(member) != null;
    }
}
