/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import entity.Account;

import javax.persistence.*;

public class LoginManager {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("gym-management");
    private EntityManager em = emf.createEntityManager();

    public Account doLogin(String username, String password) {

        String hql = "FROM Account WHERE userName = :username";
        Query query = this.em.createQuery(hql);
        query.setParameter("username", username);

        try {
            Account account = (Account) query.getSingleResult();
            return PasswordEncoder.isPasswordValid(account.getPassword(), password, account.getSalt()) ? account : null;
        } catch (NoResultException e) {
            return null;
        }
    }
}
