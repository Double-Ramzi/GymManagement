package repository;

import entity.BaseEntity;
import entity.Member;
import manager.PersistenceUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class BaseRepository<T extends BaseEntity> {

    protected EntityManager em = PersistenceUtility.getEntityManager();

    private Class<T> getEntityType() {
        return entityType;
    }

    private Class<T> entityType;

    public BaseRepository(Class<T> entityType) {
        this.entityType = entityType;
    }

    public List<T> findAll() {
        Query query = this.em.createQuery("FROM " + this.getEntityType().getSimpleName());
        List entityList = query.getResultList();

        return (entityList != null) ? entityList : new ArrayList();
    }

    public T findOne(Integer id){
       return this.em.find(this.getEntityType(), id);
    }

    public void persist(T entity) {
        this.em.getTransaction().begin();
        this.em.persist(entity);
        this.em.flush();
        this.em.getTransaction().commit();
    }

    public void persist(T entity, boolean flush) {
        this.em.getTransaction().begin();
        this.em.persist(entity);

        if (flush) {
            this.em.flush();
        }

        this.em.getTransaction().commit();
    }
}
