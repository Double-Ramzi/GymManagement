package repository;

import entity.Member;

import javax.persistence.Query;
import java.util.List;

public class MemberRepository extends BaseRepository<Member> {
    public MemberRepository() {
        super(Member.class);
    }

    public List foundSubscriptions(Member member) {
        Query query = this.em.createQuery("FROM Subscription as subscription WHERE subscription.member.id = :memberId");
        query.setParameter("memberId", member.getId());

        return query.getResultList();
    }
}