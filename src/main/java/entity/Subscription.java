package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity()
@Table()
public class Subscription extends BaseEntity implements Serializable {

    @OneToOne()
    private SubscriptionOffer subscriptionOffer;

    @OneToOne()
    private Member member;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date subscriptionDate;

    public SubscriptionOffer getSubscriptionOffer() {
        return subscriptionOffer;
    }

    public void setSubscriptionOffer(SubscriptionOffer subscriptionOffer) {
        this.subscriptionOffer = subscriptionOffer;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }
}
