package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity()
@Table()
public class HealthRecord extends BaseEntity implements Serializable {

    @ManyToOne
    private Staff staff;

    @ManyToOne
    private Member member;

    @Column(nullable = false)
    private boolean confidential;

    @Column()
    private String description;

    @Column()
    @Temporal(TemporalType.DATE)
    private Date date;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
