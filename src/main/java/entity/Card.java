package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity()
@Table()
public class Card extends BaseEntity implements Serializable {

    @OneToOne()
    private Person person;

    @Column(nullable = false)
    private Integer physicalNumber;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getPhysicalNumber() {
        return physicalNumber;
    }

    public void setPhysicalNumber(Integer physicalNumber) {
        this.physicalNumber = physicalNumber;
    }
}
