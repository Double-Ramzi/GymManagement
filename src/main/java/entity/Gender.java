package entity;

public enum Gender {

    FEMALE("F"),
    MALE("M"),
    OTHER("O");

    private String code;

    Gender(String code) {
        this.code = code;
    }

    public static Gender fromCode(String code) {
        for (Gender gender : Gender.values()) {
            if (gender.getCode().equals(code)) {
                return gender;
            }
        }
        throw new UnsupportedOperationException("The code " + code + " is not supported!");
    }

    public String getCode() {
        return code;
    }
}
