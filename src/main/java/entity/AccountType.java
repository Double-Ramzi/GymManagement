package entity;

public enum AccountType {

    MEMBER("M"),
    STAFF("S"),
    BOSS("B"),
    ADMIN("A");

    private String code;

    AccountType(String code) {
        this.code = code;
    }

    public static AccountType fromCode(String code) {
        for (AccountType accountType : AccountType.values()) {
            if (accountType.getCode().equals(code)) {
                return accountType;
            }
        }
        throw new UnsupportedOperationException("The code " + code + " is not supported!");
    }

    public String getCode() {
        return code;
    }

}
