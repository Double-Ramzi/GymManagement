package actions.fixtures;

import com.opensymphony.xwork2.ActionSupport;
import entity.SubscriptionOffer;
import manager.PersistenceUtility;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FixturesAction extends ActionSupport {

    EntityManager em = PersistenceUtility.getEntityManager();

    public String importSubscriptionOffer() {
        try {

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
            Date start = formatter.parse("01/01/2018");
            Date end = formatter.parse("01/01/2019");

            SubscriptionOffer offer1 = new SubscriptionOffer();
            offer1.setName("Basic");
            offer1.setBeginDate(start);
            offer1.setEndDate(end);
            offer1.setDescription("Basic subscription");
            offer1.setDuration(1);
            offer1.setPrice(9.99);

            SubscriptionOffer offer2 = new SubscriptionOffer();
            offer2.setName("Medium");
            offer2.setBeginDate(start);
            offer2.setEndDate(end);
            offer2.setDescription("Basic subscription");
            offer2.setDuration(1);
            offer2.setPrice(19.99);

            SubscriptionOffer offer3 = new SubscriptionOffer();
            offer3.setName("Premium");
            offer3.setBeginDate(start);
            offer3.setEndDate(end);
            offer3.setDescription("Basic subscription");
            offer3.setDuration(1);
            offer3.setPrice(29.99);

            this.em.getTransaction().begin();

            this.em.persist(offer1);
            this.em.persist(offer2);
            this.em.persist(offer3);
            this.em.flush();

            this.em.getTransaction().commit();

            return SUCCESS;


        } catch (ParseException e) {
            return ERROR;
        }
    }
}
