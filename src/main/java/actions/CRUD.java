package actions;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import entity.BaseEntity;
import manager.PersistenceUtility;
import org.apache.struts2.ServletActionContext;
import repository.BaseRepository;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public abstract class CRUD<T extends BaseEntity> extends ActionSupport {
    protected EntityManager em = PersistenceUtility.getEntityManager();
    protected T entity;
    protected int id;
    private List list;
    private BaseRepository repository;

    private Class<T> entityType;

    public CRUD(Class<T> entity) {
        this.entityType = entity;
        this.repository = new BaseRepository<>(this.getEntityType());
    }

    public String list() {
        this.list = this.repository.findAll();
        return SUCCESS;
    }

    public String create() {
        return SUCCESS;
    }
    
    public String createProcess() {
        this.persistEntity();
        return Action.SUCCESS;
    }

    public String update() {
        this.refreshEntity();
        return SUCCESS;
    }
    
    public String updateProcess() {
        this.refreshEntity();
        this.persistEntity();       
        return Action.SUCCESS;
    }
    
    protected abstract void persistEntity();
    
    public String show() {
        this.refreshEntity();

        return SUCCESS;
    }

    public String delete() {
        this.refreshEntity();

        em.getTransaction().begin();
        this.em.remove(this.entity);
        this.em.flush();
        em.getTransaction().commit();

        return SUCCESS;
    }
    
    public void setEntity(T entity) {
        this.entity = entity;
    }
    
    public T getEntity() {
        return this.entity;
    }

    private T getEntity(Integer id) {
        return this.em.find(this.getEntityType(), id);
    }

    protected void refreshEntity() {
        this.entity = this.getEntity(this.id);
    }

    private Class<T> getEntityType() {
        return this.entityType;
    }

    public List getList() {
        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();
    }
}
