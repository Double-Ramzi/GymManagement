/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.account;

import com.opensymphony.xwork2.Action;
import entity.*;
import manager.PasswordEncoder;
import manager.PersistenceUtility;

import javax.persistence.EntityManager;
import java.util.Date;


public class UserAction {

    private EntityManager em = PersistenceUtility.getEntityManager();
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private Date birthdate;
    private String phoneNumber;
    private String phoneEmergency;
    private String adress;
    private String email;
    private String gender;


    public String registerSucess() {

        Account account = new Account();
        Person person = new Member();
        account.setPerson(person);
        account.setAccountType(AccountType.MEMBER);
        account.setUserName(this.username);
        account.setSalt(PasswordEncoder.generateSalt());

        account.setPassword(PasswordEncoder.encodePassword(this.password, account.getSalt()));

        person.setFirstName(firstname);
        person.setLastName(lastname);
        person.setBirthdate(birthdate);
        person.setPhoneNumber(phoneNumber);
        person.setPhoneEmergency(phoneEmergency);
        person.setAddress(adress);
        person.setEmail(email);

        person.setGender(Gender.fromCode(gender));

        this.em.getTransaction().begin();
        this.em.persist(person);
        this.em.persist(account);
        this.em.flush();
        this.em.getTransaction().commit();
        this.em.close();

        return Action.SUCCESS;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPhoneEmergency(String phoneEmergency) {
        this.phoneEmergency = phoneEmergency;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
