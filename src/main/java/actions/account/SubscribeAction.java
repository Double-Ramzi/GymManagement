/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.account;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import entity.Account;
import entity.Member;
import entity.Subscription;
import entity.SubscriptionOffer;
import manager.PersistenceUtility;
import org.apache.struts2.ServletActionContext;
import repository.BaseRepository;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

public class SubscribeAction extends ActionSupport {

    private Integer idOffer;

    private EntityManager em = PersistenceUtility.getEntityManager();

    public String execute() {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();

        Account account = (Account) session.getAttribute("account");

        if (account == null) {

            return Action.ERROR;
        } else {

            BaseRepository<SubscriptionOffer> subscriptionOfferBaseRepository = new BaseRepository<>(SubscriptionOffer.class);
            BaseRepository<Member> memberBaseRepository = new BaseRepository<>(Member.class);

            SubscriptionOffer subscriptionOffer = subscriptionOfferBaseRepository.findOne(this.idOffer);
            Subscription subscription = new Subscription();

            Member member = memberBaseRepository.findOne(account.getPerson().getId());

            if (member != null) {

                subscription.setMember(member);
            } else {

                return Action.ERROR;
            }

            subscription.setSubscriptionOffer(subscriptionOffer);
            subscription.setSubscriptionDate(new Date());

            em.getTransaction().begin();
            em.persist(subscription);
            em.flush();
            em.getTransaction().commit();

            return SUCCESS;
        }

    }

    public Integer getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(Integer idOffer) {
        this.idOffer = idOffer;
    }
}
