package actions.account;

import com.opensymphony.xwork2.Action;
import entity.Account;
import manager.LoginManager;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class LoginAction implements SessionAware {

    private String username;
    private String password;
    private SessionMap<String, Object> sessionmap;

    private String error;

    public String execute() {

        // TODO : check user credentials
        LoginManager loginManager = new LoginManager();
        Account account = loginManager.doLogin(this.getUsername(), this.getPassword());

        if (account != null) {
            this.sessionmap.put("account", account);

            return Action.SUCCESS;
        } else {

            this.error = "Combinaison incorrecte !";
            return Action.ERROR;
        }
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getError() {
        return error;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.sessionmap = (SessionMap<String, Object>) map;
    }

    public String logout() {
        this.sessionmap.invalidate();
        return Action.SUCCESS;
    }
}
