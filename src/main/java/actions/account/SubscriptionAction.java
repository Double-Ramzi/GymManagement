/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.account;

import actions.CRUD;
import com.opensymphony.xwork2.ActionSupport;
import entity.Member;
import entity.Subscription;
import entity.SubscriptionOffer;
import repository.BaseRepository;

public class SubscriptionAction extends CRUD {
    Integer idOffer;

    public SubscriptionAction() {
        super(SubscriptionOffer.class);
    }

    public void setId(Integer id) {
        this.idOffer = id;
    }

    public String execute() {

        BaseRepository<SubscriptionOffer> repo = new BaseRepository<>(SubscriptionOffer.class);

        Subscription subscription = new Subscription();

        Member member = new Member();
        member.setFirstName("Jean");
        member.setLastName("Tremblay");
        member.setPhoneNumber("4181234567");
        member.setEmail("jean@videotron.ca");
        member.setAddress("123 rue du quebec, chiocutimi");

        subscription.setMember(member);
        subscription.setSubscriptionOffer(repo.findOne(idOffer));


        return SUCCESS;
    }

    @Override
    public String updateProcess() {
        return null;
    }

    @Override
    protected void persistEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
