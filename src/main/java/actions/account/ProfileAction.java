/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.account;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import entity.Account;
import entity.Member;
import entity.Subscription;
import entity.SubscriptionOffer;
import manager.MemberManager;
import org.apache.struts2.ServletActionContext;
import repository.BaseRepository;
import repository.MemberRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ProfileAction extends ActionSupport {

    private BaseRepository<SubscriptionOffer> repository = new BaseRepository<>(SubscriptionOffer.class);
    private MemberRepository memberRepository = new MemberRepository();
    private MemberManager memberManager = new MemberManager();

    public List<SubscriptionOffer> getOffers() {
        return offers;
    }

    private List<SubscriptionOffer> offers;

    private Subscription currentSubscription;

    private Member member;

    public String execute() {
        this.offers = this.repository.findAll();

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();

        Account account = (Account) session.getAttribute("account");

        if (account != null) {
            this.member = this.memberRepository.findOne(account.getPerson().getId());
            if (member != null) {
                this.currentSubscription = this.memberManager.getCurrentSubscription(this.member);
                return Action.SUCCESS;
            }
        }

        return Action.ERROR;
    }

    public Subscription getCurrentSubscription() {
        return currentSubscription;
    }

    public Member getMember() {
        return member;
    }

}
