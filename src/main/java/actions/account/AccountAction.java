/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.account;

import actions.CRUD;
import entity.Account;

public class AccountAction extends CRUD {
    
    public AccountAction() {
        super(Account.class);
    }

    public void setAccount(Account account) {
        this.entity = account;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    protected void persistEntity() {}

}
