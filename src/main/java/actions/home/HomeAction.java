package actions.home;

import com.opensymphony.xwork2.ActionSupport;
import entity.SubscriptionOffer;
import repository.BaseRepository;

import java.util.ArrayList;
import java.util.List;

public class HomeAction extends ActionSupport {
    public List<SubscriptionOffer> getOffers() {
        return offers;
    }

    private List<SubscriptionOffer> offers = new ArrayList<>();

    public String execute() {
        BaseRepository<SubscriptionOffer> repository = new BaseRepository<>(SubscriptionOffer.class);
        this.offers = repository.findAll();

        return SUCCESS;
    }

}