/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions.subscription.offer;

import actions.CRUD;
import entity.SubscriptionOffer;
import java.util.Date;

public class SubscriptionOfferAction extends CRUD {
    
    private String name;
    private double price;
    private int duration;
    private String description;
    private Date beginDate;
    private Date endDate;

    public SubscriptionOfferAction() {
        super(SubscriptionOffer.class);
    }
    
    @Override
    protected void persistEntity() {
        SubscriptionOffer entity;
        if (this.entity == null) {
            entity = new SubscriptionOffer();
        } else {
            entity = (SubscriptionOffer) this.entity;
        }
        
        entity.setName(this.name);
        entity.setPrice(this.price);
        entity.setDuration(this.duration);
        entity.setDescription(this.description);
        entity.setBeginDate(this.beginDate);
        entity.setEndDate(this.endDate);
        
        em.getTransaction().begin();
        this.em.persist(entity);
        this.em.flush();
        em.getTransaction().commit();
        
        this.entity = entity;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public String getDescription() {
        return description;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
