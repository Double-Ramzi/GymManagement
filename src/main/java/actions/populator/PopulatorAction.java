package actions.populator;

import com.opensymphony.xwork2.ActionSupport;


public class PopulatorAction extends ActionSupport {

    public String execute() {
        System.out.println("Dans le populator action");


        Populator populator = new Populator();
        System.out.println(populator.populateRandomMember(10));
        System.out.println(populator.populateRandomStaff(10));

        return SUCCESS;
    }
}