package actions.populator;

import entity.Member;
import entity.Staff;
import manager.PersistenceUtility;

import javax.persistence.EntityManager;


    public class Populator {


        EntityManager em = PersistenceUtility.getEntityManager();

        String[] firstName = {"Jean", "jeanne", "jeanie", "joanie"};
        String[] lastName = {"Tremblay", "Gauthier", "Fortin"};
        String[] city = {"Chicoutimi", "Jonquiere", "Alma", "Quebec", "Montreal"};


        public String populateRandomMember(int nb){

            for(int i = 0; i<nb ; i++){
                Member member = new Member();
                member.setFirstName(firstName[(int)(Math.random()*3)]);
                member.setLastName(lastName[(int)(Math.random()*2)]);
                member.setPhoneNumber("418 123 " + (nb*1021));
                member.setEmail(firstName[(int)(Math.random()*3)]+"@videotron.ca");
                member.setAddress((int)(Math.random()*299)+" rue du quebec, "+city[(int) Math.random()*4]);

                this.em.getTransaction().begin();
                this.em.persist(member);
                this.em.flush();
                this.em.getTransaction().commit();
                this.em.close();

            }

            return "populated";
        }


        public String populateRandomStaff(int nb){

            for(int i = 0; i<nb ; i++){
                Staff staff = new Staff();
                staff.setFirstName(firstName[(int)(Math.random()*3)]);
                staff.setLastName(lastName[(int)(Math.random()*2)]);
                staff.setPhoneNumber("418 321 " + (nb*1021));
                staff.setEmail(firstName[(int)(Math.random()*3)]+"@megagym.ca");
                staff.setAddress((int)(Math.random()*299)+" rue jacques cartier, "+city[(int) Math.random()*4]);

                this.em.getTransaction().begin();
                this.em.persist(staff);
                this.em.flush();
                this.em.getTransaction().commit();
                this.em.close();

            }

            return "populated";
        }


        public String populateMember(){

            Member member = new Member();
            member.setFirstName("Jean");
            member.setLastName("Tremblay");
            member.setPhoneNumber("4181234567");
            member.setEmail("jean@videotron.ca");
            member.setAddress("123 rue du quebec, chiocutimi");

            this.em.getTransaction().begin();
            this.em.persist(member);
            this.em.flush();
            this.em.getTransaction().commit();
            this.em.close();

            return "populated";
        }
    }

