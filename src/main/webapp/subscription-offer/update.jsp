<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>Gym - SubscriptionOffer Update</title>
        <jsp:include page="../layout/css_import.jsp"/>
        
        <link href="../node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>

        <section class="section-dark-grey">
            <div class="d-flex align-items-center flex-column justify-content-center h-100">
                <s:form action="update-process" method="POST">
                        <s:hidden name="id" value="%{entity.id}"/>
                        <div class="form-group">
                            <s:textfield class="form-control form-control-lg" type="text" label="Nom" placeholder="Nom" name="name" value="%{entity.name}"/>
                        </div>
                        <div class="form-group">
                            <s:textfield class="form-control form-control-lg" label="Price" placeholder="Price" name="price" value="%{entity.price}"/>
                        </div>
                        <div class="form-group">
                            <s:textfield class="form-control form-control-lg" label="Duration" placeholder="Duration" name="duration" value="%{entity.duration}"/>
                        </div>
                        <div class="form-group">
                            <s:textarea class="form-control form-control-lg" label="Description" placeholder="Description" name="description" value="%{entity.description}"/>
                        </div>
                        <div id="daterange_container">
                            <div class="input-daterange input-group" id="datepicker">
                                <s:textfield class="datepicker input-sm form-control" label="Du" name="beginDate" value="%{entity.beginDate}"/>
                                <s:textfield class="datepicker input-sm form-control" label="au" name="endDate" value="%{entity.endDate}"/>
                            </div>
                        </div>
                    <s:submit class="button" value="Update"/>
                </s:form>
            </div>
        </section>

        <section></section>

        <jsp:include page="../layout/footer.jsp"/>
        <jsp:include page="../layout/js_import.jsp"/>
        
        <script src="../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        
        <script>
            $(document).ready(function () {
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    language: "fr"
                });
            });
        </script>
    </body>
</html>