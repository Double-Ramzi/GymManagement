<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>JSP Page</title>
        <jsp:include page="../layout/css_import.jsp"/>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>

        <section class="section-dark-grey">
            <div class="d-flex align-items-center flex-column justify-content-center h-100">
                <s:form action="register-process">

                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Prenom" type="text"
                               name="firstname">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Nom" type="text"
                               name="lastname">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Date de naissance" type="date"
                               name="birthdate">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Téléphone" type="text"
                               name="phoneNumber">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Téléphone d'urgence" type="text"
                               name="phoneEmergency">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Adresse" type="text"
                               name="adress">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Couriel" type="text"
                               name="email">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Genre (M,F,O)" type="text"
                               name="gender">
                    </div>

                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Nom d'utilisateur" type="text"
                               name="username">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" placeholder="Mot de passe" type="password"
                               name="password">
                    </div>
                    <s:submit class="button" value="S'inscrire"/>
                </s:form>
            </div>

        </section>

        <section></section>

        <jsp:include page="../layout/footer.jsp"/>
        <jsp:include page="../layout/js_import.jsp"/>
    </body>
</html>
