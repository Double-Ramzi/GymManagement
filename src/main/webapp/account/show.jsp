<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>Gym - Account Show</title>
        <jsp:include page="../layout/css_import.jsp"/>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>

        <section>
            <div class="container">
                <h2>Account</h2>
                <table class="table table-bordered">
                    <tr>
                        <th>Username</th>
                        <td><s:property value="entity.userName"/></td>
                    </tr>
                    <tr>
                        <th>AccountType</th>
                        <td><s:property value="entity.accountType"/></td>
                    </tr>
                    <tr>
                        <th>Person</th>
                        <td>
                            <s:property value="entity.person.firstName"/>
                            <s:property value="entity.person.lastName"/>
                        </td>
                        
                    </tr>
                </table>
            </div>
        </section>

        <section></section>

        <jsp:include page="../layout/footer.jsp"/>
        <jsp:include page="../layout/js_import.jsp"/>
    </body>
</html>
