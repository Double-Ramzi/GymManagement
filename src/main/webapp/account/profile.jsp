<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html lang="fr">
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>Gym</title>
        <jsp:include page="../layout/css_import.jsp"/>
    </head>

    <body class="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>
        <section>
            <div class="container">
                <div class="row">
                    <table class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Date de naissance</th>
                                <th>Numero de téléphone</th>
                                <th>Adresse</th>
                                <th>Courriel</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><s:property value="member.lastName"/></td>
                                <td><s:property value="member.firstName"/></td>
                                <td><s:property value="member.birthDate"/></td>
                                <td><s:property value="member.phoneNumber"/></td>
                                <td><s:property value="member.address"/></td>
                                <td><s:property value="member.email"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <s:if test="currentSubscription == null">

            <section id="subscriptions">
                <div class="container">
                    <div class="row">
                        <s:iterator value="offers" status="ctr">
                            <s:if test="#ctr.index < 3">
                                <div class="col-md-4 col-sm-12">
                                    <div class="plan">
                                        <div class="plan-title">
                                            <s:property value="name"/>
                                        </div>
                                        <div class="plan-cost">
                                    <span class="plan-price">
                                        <s:property value="price"/> $
                                    </span>
                                            <span class="plan-type">/ <s:property value="duration"/> mois</span>
                                        </div>
                                        <ul class="plan-features">
                                            <li>Flexible : résiliez quand vous voulez</li>
                                            <li>Paiement tout les <s:property value="duration"/> mois</li>
                                            <li>Résiliable mensuellement dès le 1er mois</li>
                                        </ul>
                                        <div class="plan-select">
                                            <a class="button-link"
                                               href="<s:url action="subscription-process" />?idOffer=<s:property value="id"/>">Souscrire</a>
                                        </div>
                                    </div>
                                </div>
                            </s:if>
                        </s:iterator>
                    </div>
                </div>
            </section>
        </s:if>
        <s:else>
            <section id="subscriptions">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="plan">
                                <div class="plan-title">
                                    <s:property value="currentSubscription.subscriptionOffer.name"/>
                                </div>
                                <div class="plan-cost">
                                    <span class="plan-price">
                                        <s:property value="currentSubscription.subscriptionOffer.price"/> $
                                    </span>
                                    <span class="plan-type">/ <s:property
                                            value="currentSubscription.subscriptionOffer.duration"/> mois</span>
                                </div>
                                <ul class="plan-features">
                                    <li>Flexible : résiliez quand vous voulez</li>
                                    <li>Paiement tout les <s:property
                                            value="currentSubscription.subscriptionOffer.duration"/> mois
                                    </li>
                                    <li>Résiliable mensuellement dès le 1er mois</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <dic class="card">
                                <div class="card-header">
                                    Votre offre en cours !
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        Vous avez souscrit le
                                        <s:property value="currentSubscription.subscriptionDate"/>.<br>
                                    </p>
                                    <p class="card-text">
                                        La durée de votre souscription est de
                                        <s:property value="currentSubscription.subscriptionOffer.duration"/> mois.
                                    </p>
                                </div>
                            </dic>
                        </div>
                    </div>
            </section>
        </s:else>
    </body>
</html>
