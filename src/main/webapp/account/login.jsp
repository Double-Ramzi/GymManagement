<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>JSP Page</title>
        <jsp:include page="../layout/css_import.jsp"/>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>

        <section class="section-dark-grey">
            <div class="d-flex align-items-center flex-column justify-content-center h-100">
                <s:if test="error != null">
                    <div class="alert alert-danger" role="alert">
                        <s:property value="error"/>
                    </div>
                </s:if>

                <s:form action="login-process">
                    <div class="form-group">
                        <label for="username">
                            Nom d'utilisateur</label>
                        <input id="username"
                               class="form-control form-control-lg"
                               placeholder="Nom d'utilisateur"
                               type="text"
                               name="username">
                    </div>
                    <div class="form-group">
                        <label for="password">Mots de passe</label>
                        <input id="password"
                               class="form-control form-control-lg"
                               placeholder="Mot de passe"
                               type="password"
                               name="password">
                    </div>
                    <s:submit class="button" value="Se connecter"/>
                </s:form>
            </div>
        </section>

        <section></section>

        <jsp:include page="../layout/footer.jsp"/>
        <jsp:include page="../layout/js_import.jsp"/>
    </body>
</html>
