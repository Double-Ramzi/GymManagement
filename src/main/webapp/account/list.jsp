<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>Gym - Account List</title>
        <jsp:include page="../layout/css_import.jsp"/>
        <jsp:include page="../layout/css_datatables.jsp"/>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <s:url action="" namespace="/" var="indexURL"/>
                <s:a class="navbar-brand js-scroll-trigger" href="%{indexURL}">Gym</s:a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <section></section>

        <section>
            <div class="container">
                <h2>
                    Account list
                    <a class="btn btn-primary float-right" href="<s:url action="create" />"><i class="fa fa-plus"></i> Create</a>
                </h2>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>AccountType</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator value="list">
                            <tr>
                                <td></td>
                                <td><s:property value="accountType"/></td>
                                <td>
                                    <a class="btn btn-default" href="<s:url action="show" />?id=<s:property value="id"/>"><i class="fa fa-eye"></i> Show</a>
                                    <a class="btn btn-default" href="<s:url action="update" />?id=<s:property value="id"/>"><i class="fa fa-edit"></i> Edit</a>
                                    <a class="btn btn-default" href="<s:url action="delete" />?id=<s:property value="id"/>"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        </s:iterator>
                    </tbody>
                </table>
            </div>
        </section>

        <section></section>

        <jsp:include page="../layout/footer.jsp"/>
        <jsp:include page="../layout/js_import.jsp"/>
        <jsp:include page="../layout/js_datatables.jsp"/>
    </body>
</html>
