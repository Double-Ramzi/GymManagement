<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html lang="fr">
    <head>
        <jsp:include page="../layout/meta.jsp"/>
        <title>Gym</title>
        <jsp:include page="../layout/css_import.jsp"/>
    </head>

    <body id="page-top">
        <jsp:include page="../layout/header.jsp"/>

        <header>
            <div id="header-carousel" class="carousel slide" data-ride="carousel">
                <ul class="carousel-indicators">
                    <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#header-carousel" data-slide-to="1"></li>
                    <li data-target="#header-carousel" data-slide-to="2"></li>
                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="../img/240382-P322G8-660.jpg" alt="Los Angeles">
                        <div class="carousel-caption">
                            <h3>Nam et ipsa scientia potestas est</h3>
                            <p>"Savoir, c'est pouvoir."</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../img/240334-P322BQ-852.jpg" alt="Chicago">
                        <div class="carousel-caption">
                            <h3>Natura non nisi parendo vincitur.</h3>
                            <p>"On ne commande à la nature qu'en lui obéissant."</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#header-carousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#header-carousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </header>

        <section id="subscriptions">
            <div class="container">
                <div class="row">
                    <s:iterator value="offers" status="ctr">
                        <s:if test="#ctr.index < 3">
                            <div class="col-md-4 col-sm-12">
                                <div class="plan">
                                    <div class="plan-title">
                                        <s:property value="name"/>
                                    </div>
                                    <div class="plan-cost">
                                    <span class="plan-price">
                                        <s:property value="price"/> $
                                    </span>
                                        <span class="plan-type">/ <s:property value="duration"/> mois</span>
                                    </div>
                                    <ul class="plan-features">
                                        <li>Flexible : résiliez quand vous voulez</li>
                                        <li>Paiement tout les <s:property value="duration"/> mois</li>
                                        <li>Résiliable mensuellement dès le 1er mois</li>
                                    </ul>
                                    <div class="plan-select">
                                        <a class="button-link" href="">Selectionner l'offre</a>
                                    </div>
                                </div>
                            </div>
                        </s:if>
                    </s:iterator>
                </div>
            </div>
        </section>

        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <img src="../img/240315-P32282-480.jpg" alt="image" class="img-fluid">
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <div class="text-box-column">
                            <h2>About this page</h2>
                            <p class="lead">This is a great place to talk about your webpage. This template is
                                purposefully unstyled so you can use it as a boilerplate or starting point for you own
                                landing page designs! This template features:</p>
                        </div>
                        <ul>
                            <li>Clickable nav links that smooth scroll to page sections</li>
                            <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                            <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in
                                the navbar
                            </li>
                            <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="middle" class="section-dark-grey">
            <div class="container text-box-column">
                <h2>Middle</h2>
                <p>Etiam pulvinar sagittis tortor eleifend dignissim. Nunc vitae aliquet massa. Integer rutrum faucibus
                    dignissim. Nullam lacinia volutpat orci a maximus. Sed ac libero pulvinar, efficitur lorem at, porta
                    dolor. Proin eu facilisis lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Pellentesque a tellus est. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Donec nunc arcu, rhoncus sit amet varius ut, commodo vel diam. Duis venenatis nisl ut nulla
                    sagittis mattis. Vivamus erat enim, consequat at facilisis non, commodo a leo.</p>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="text-box-column">
                            <h2>Services we offer</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit
                                inventore, expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur
                                ex. Nemo assumenda laborum vel, labore ut velit dignissimos.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <img src="../img/240327-P322AV-896.jpg" alt="image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>

        <section id="contact" class="section-dark-grey">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <h2>Contact us</h2>
                        <div class="form-area">
                            <form role="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                           required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email"
                                           required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" name="mobile"
                                           placeholder="Mobile Number" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject"
                                           placeholder="Subject" required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" id="message" placeholder="Message"
                                              maxlength="140" rows="7"></textarea>
                                </div>

                                <button type="submit" class="button" id="submit" name="submit">Submit Form</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <footer class="py-5">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
            </div>
            <!-- /.container -->
        </footer>

        <jsp:include page="../layout/js_import.jsp"/>
    </body>

</html>
